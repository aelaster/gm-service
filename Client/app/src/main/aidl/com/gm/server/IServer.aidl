package com.gm.server;
import com.gm.server.IServerCallback;

interface IServer {
    void getCurrent_LatLng(double latitude, double longitude, IServerCallback cb);
    void getCurrent_CityName(String city_name, IServerCallback cb);
}