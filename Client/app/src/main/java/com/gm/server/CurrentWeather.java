package com.gm.server;

import android.os.Parcel;
import android.os.Parcelable;

public class CurrentWeather implements Parcelable {
  public String httpCode;
  public String httpMessage;
  public String cityName;
  public String currentTemp;
  public String currentConditions;
  public String weatherIcon;

  public CurrentWeather(String httpCode, String httpMessage, String cityName, String currentTemp, String currentConditions, String weatherIcon) {
    this.httpCode = cityName;
    this.httpMessage = cityName;
    this.cityName = cityName;
    this.currentTemp = currentTemp;
    this.currentConditions = currentConditions;
    this.weatherIcon = weatherIcon;
  }

  public CurrentWeather() {
  }

  protected CurrentWeather(Parcel in) {
    httpCode = in.readString();
    httpMessage = in.readString();
    cityName = in.readString();
    currentTemp = in.readString();
    currentConditions = in.readString();
    weatherIcon = in.readString();
  }

  public static final Creator<CurrentWeather> CREATOR = new Creator<CurrentWeather>() {
    @Override
    public CurrentWeather createFromParcel(Parcel in) {
      return new CurrentWeather(in);
    }

    @Override
    public CurrentWeather[] newArray(int size) {
      return new CurrentWeather[size];
    }
  };

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel parcel, int i) {
    parcel.writeString(httpCode);
    parcel.writeString(httpMessage);
    parcel.writeString(cityName);
    parcel.writeString(currentTemp);
    parcel.writeString(currentConditions);
    parcel.writeString(weatherIcon);
  }
}