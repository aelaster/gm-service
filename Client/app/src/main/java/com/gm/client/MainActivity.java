package com.gm.client;

import android.app.Activity;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.text.method.DigitsKeyListener;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gm.server.CurrentWeather;
import com.gm.server.IServer;
import com.gm.server.IServerCallback;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

  private TextView txtCityName, txtCurrentTemperature, txtCurrentConditions;
  private EditText inputLatitude, inputLongitude, inputCityName;
  private ImageView imgWeatherIcon;
  private ProgressBar pbProgress;
  private String Tag = "Client";
  private Button btnGetWeather;

  protected IServer weatherService;

  private IServerCallback callback = new IServerCallback.Stub() {
    @Override
    public void callback(final CurrentWeather output) {
      //AIDL callbacks are on the binder thread
      runOnUiThread(new Runnable() {
        @Override
        public void run() {
          pbProgress.setVisibility(View.GONE);
          btnGetWeather.setEnabled(true);

          if (output != null) {
            if (output.httpCode.equals("200")) {
              txtCityName.setText(output.cityName);
              txtCurrentTemperature.setText(output.currentTemp);
              txtCurrentConditions.setText(output.currentConditions);

              Picasso.with(MainActivity.this).load(output.weatherIcon).into(imgWeatherIcon);
            }else{
              Toast toast = Toast.makeText(MainActivity.this, getString(R.string.error_output,output.httpCode,output.httpMessage), Toast.LENGTH_SHORT);
              toast.show();
            }
          }else{
            Toast toast = Toast.makeText(MainActivity.this, getString(R.string.server_connection_error), Toast.LENGTH_SHORT);
            toast.show();
          }
        }
      });
    }
  };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    pbProgress = findViewById(R.id.pbProgress);

    btnGetWeather = findViewById(R.id.btnGetWeather);
    btnGetWeather.setOnClickListener(this);

    Button btnClearWeather = findViewById(R.id.btnClearWeather);
    btnClearWeather.setOnClickListener(this);

    Button btnClearForm = findViewById(R.id.btnClearForm);
    btnClearForm.setOnClickListener(this);

    inputLatitude = findViewById(R.id.inputLatitude);
    inputLatitude.setKeyListener(DigitsKeyListener.getInstance(true,true));
    inputLongitude = findViewById(R.id.inputLongitude);
    inputLongitude.setKeyListener(DigitsKeyListener.getInstance(true,true));
    inputCityName = findViewById(R.id.inputCityName);

    txtCityName = findViewById(R.id.txtCityName);
    txtCurrentTemperature = findViewById(R.id.txtCurrentTemperature);
    txtCurrentConditions = findViewById(R.id.txtCurrentConditions);
    imgWeatherIcon = findViewById(R.id.imgWeatherIcon);

    initConnection();
  }

  @Override
  protected void onResume() {
    super.onResume();
    if (weatherService == null) {
      initConnection();
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    unbindService(serviceConnection);
  }

  private void initConnection() {
    if (weatherService == null) {
      Intent intent = new Intent(IServer.class.getName());

      /*this is service name*/
      intent.setAction(getString(R.string.server_service_name));

      /*From 5.0 annonymous intent calls are suspended so replacing with server app's package name*/
      intent.setPackage(getString(R.string.server_app_uri));

      // binding to remote service
      bindService(intent, serviceConnection, Service.BIND_AUTO_CREATE);
    }
  }

  private ServiceConnection serviceConnection = new ServiceConnection() {
    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
      Log.d(Tag, "Service Connected");
      weatherService = IServer.Stub.asInterface(iBinder);
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
      Log.d(Tag, "Service Disconnected");
      weatherService = null;
    }
  };



  @Override
  public void onClick(View view) {
    hideKeyboard(this);
    clearWeather();
    String serverAppUri = getString(R.string.server_app_uri);
    if (appInstalledOrNot(serverAppUri)) {
      switch (view.getId()) {

        case R.id.btnGetWeather:
          try {
            btnGetWeather.setEnabled(false);
            pbProgress.setVisibility(View.VISIBLE);

            String latitude = inputLatitude.getText().toString();
            String longitude = inputLongitude.getText().toString();
            String city_name = inputCityName.getText().toString();

            if (!city_name.equals("")) {
              weatherService.getCurrent_CityName(city_name, callback);
            }else if (!latitude.equals("") && !longitude.equals("")){
              weatherService.getCurrent_LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude), callback);
            }else{
              Toast toast = Toast.makeText(this, getString(R.string.complete_form), Toast.LENGTH_SHORT);
              pbProgress.setVisibility(View.GONE);
              btnGetWeather.setEnabled(true);
              toast.show();
            }
          } catch (RemoteException e) {
            e.printStackTrace();
            Log.d(Tag, getString(R.string.server_connection_error));
            Toast toast = Toast.makeText(this, getString(R.string.server_connection_error), Toast.LENGTH_SHORT);
            toast.show();
          }
          break;

        case R.id.btnClearWeather:
          break;

        case R.id.btnClearForm:
          clearForm();
          break;
      }
    } else {
      clearForm();
      Toast.makeText(MainActivity.this, getString(R.string.server_missing), Toast.LENGTH_SHORT).show();
    }
  }

  private void clearForm(){
    inputLatitude.setText("");
    inputLongitude.setText("");
    inputCityName.setText("");
  }

  private void clearWeather(){
    txtCityName.setText("");
    txtCurrentTemperature.setText("");
    txtCurrentConditions.setText("");
    imgWeatherIcon.setImageDrawable(null);
  }

  private boolean appInstalledOrNot(String uri) {
    PackageManager pm = getPackageManager();
    boolean app_installed;
    try {
      pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
      app_installed = true;
    } catch (PackageManager.NameNotFoundException e) {
      app_installed = false;
    }
    return app_installed;
  }

  private void hideKeyboard(Activity activity) {
    InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
    //Find the currently focused view, so we can grab the correct window token from it.
    View view = activity.getCurrentFocus();
    //If no view currently has focus, create a new one, just so we can grab a window token from it
    if (view == null) {
      view = new View(activity);
    }

    if (imm != null) {
      imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
  }
}