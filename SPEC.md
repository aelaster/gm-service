# Demo Design Spec
- Build an Android service (AIDL) that exposes APIs that map to a RESTFUL endpoint.  For example, a weather service.
- Build an Android application that binds to the AIDL and uses the AIDL APIs to pull and render content from the endpoint.