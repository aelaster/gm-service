package com.gm.server;
import com.gm.server.CurrentWeather;

interface IServerCallback {
    void callback(in CurrentWeather output);
}
