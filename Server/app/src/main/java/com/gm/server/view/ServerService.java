package com.gm.server.view;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import com.gm.server.App;
import com.gm.server.CurrentWeather;
import com.gm.server.IServer;
import com.gm.server.IServerCallback;
import com.gm.server.R;
import com.gm.server.model.Weather;
import com.gm.server.model.WeatherInfo;
import com.gm.server.presenter.WeatherPresenter;

import java.util.ArrayList;

import javax.inject.Inject;

public class ServerService extends Service implements ServerServiceView {

  private WeatherPresenter mPresenter;

  @Override
  public void onCreate() {

    ((App) getApplicationContext()).getAppComponent().inject(this);
      mPresenter.bind(this);
      super.onCreate();
  }

  @Override
  public void onDestroy() {
      mPresenter.unbind();
      super.onDestroy();
  }

  @Inject
  public void setPresenter(WeatherPresenter presenter) {
    this.mPresenter = presenter;
  }

  @Nullable
  @Override
  public IBinder onBind(Intent intent) {
    return mBinder;
  }

  private final IServer.Stub mBinder = new IServer.Stub() {
      @Override
      public void getCurrent_LatLng(double latitude, double longitude, IServerCallback cb) {
          mPresenter.getCurrent_LatLng(latitude, longitude, cb);
      }

      @Override
      public void getCurrent_CityName(String city_name, IServerCallback cb) {
          mPresenter.getCurrent_CityName(city_name, cb);
      }
  };

    @Override
    public void updateUi(Weather weather, IServerCallback cb) {
        try {
            if (weather != null) {
                CurrentWeather output = new CurrentWeather();
                if (weather.getHttp_code().equals("200")) {
                    output.httpCode = weather.getHttp_code();
                    output.httpMessage = getString(R.string.http_ok);

                    output.cityName = getString(R.string.cityNameCountry, weather.getCityName(), weather.getSysInfo().getCountry());
                    output.currentTemp = weather.getTemperatureInfo().getTemprature(getApplicationContext());

                    ArrayList<WeatherInfo> weather_info = weather.getWeatherInfo();
                    output.currentConditions = getString(R.string.conditions, weather_info.get(0).getDescription(), weather_info.get(0).getDescriptionDetail());
                    output.weatherIcon = getString(R.string.icon_url, weather_info.get(0).getIcon());
                }else{
                    output.httpCode = weather.getHttp_code();
                    output.httpMessage = weather.getHttp_message();
                }
                cb.callback(output);
            }else{
                cb.callback(null);
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}