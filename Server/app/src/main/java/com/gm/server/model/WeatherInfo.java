package com.gm.server.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WeatherInfo {
    @SerializedName("main")
    @Expose
    private String description = "";

    @SerializedName("description")
    @Expose
    private String description_detail = "";

    @SerializedName("icon")
    @Expose
    private String icon = "";

    public String getDescription() {
        return description;
    }

    public String getDescriptionDetail() {
        return description_detail;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
