package com.gm.server.presenter;

import com.gm.server.IServerCallback;
import com.gm.server.api.WeatherAPIInteractor;
import com.gm.server.model.Weather;
import com.gm.server.view.ServerServiceView;
import com.google.gson.Gson;
import org.jetbrains.annotations.NotNull;
import java.io.IOException;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherPresenter {
    private WeatherAPIInteractor mInteractor;
    private ServerServiceView mServerServiceView;

    @Inject
    WeatherPresenter(WeatherAPIInteractor interactor) {
        this.mInteractor = interactor;
    }

    public void bind(ServerServiceView view) { this.mServerServiceView = view; }
    public void unbind() {
        mServerServiceView = null;
    }


    public void getCurrent_LatLng(double lat, double lng, final IServerCallback cb) {
        mInteractor.getCurrent_LatLng(lat, lng)
                .enqueue(new WeatherCallback(cb));
    }

    public void getCurrent_CityName(String city_name, IServerCallback cb) {
        mInteractor.getCurrent_CityName(city_name)
            .enqueue(new WeatherCallback(cb));
    }

    private class WeatherCallback implements Callback<Weather> {
        IServerCallback callback;

        WeatherCallback( IServerCallback cb ) {
            this.callback = cb;
        }

        @Override
        public void onResponse(@NotNull Call<Weather> call,
                               @NotNull Response<Weather> weather) {
            if (mServerServiceView != null)
                mServerServiceView.updateUi(errorCheck(weather), callback);
        }

        @Override
        public void onFailure(@NotNull Call<Weather> call, @NotNull Throwable t) {
            t.printStackTrace();
            if (mServerServiceView != null){
                mServerServiceView.updateUi(null, callback);
            }
        }
    }

    private Weather errorCheck(Response<Weather> output){
        try {
            ResponseBody errorBody = output.errorBody();
            if (errorBody != null) {
                Gson gson = new Gson();

                ErrorResponse response = gson.fromJson(errorBody.string(), ErrorResponse.class);

                Weather errorResponse = new Weather();
                errorResponse.setHttp_code(response.cod);
                errorResponse.setHttp_message(response.message);

                return errorResponse;
            } else {
                return output.body();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    class ErrorResponse{
        String cod;
        String message;
    }
}
