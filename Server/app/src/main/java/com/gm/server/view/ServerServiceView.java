package com.gm.server.view;

import com.gm.server.IServerCallback;
import com.gm.server.model.Weather;

public interface ServerServiceView {
    void updateUi(Weather current, IServerCallback callback);
}
