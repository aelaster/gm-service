package com.gm.server;

import com.gm.server.api.WeatherModule;
import com.gm.server.view.ServerService;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = WeatherModule.class)
public interface AppComponent {
    void inject(ServerService service);
}
