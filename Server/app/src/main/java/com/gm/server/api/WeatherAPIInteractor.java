package com.gm.server.api;

import com.gm.server.model.Weather;

import retrofit2.Call;

public interface WeatherAPIInteractor {
    Call<Weather> getCurrent_LatLng(double lat, double lng);
    Call<Weather> getCurrent_CityName(String city_name);
}
