package com.gm.server.api;

import com.gm.server.model.Weather;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherAPI {
    @GET("weather")
    Call<Weather> getCurrent_LatLng(@Query("lat") double latitude, @Query("lon") double longitude, @Query("appid") String apiKey, @Query("units") String units);

    @GET("weather")
    Call<Weather> getCurrent_CityName(@Query("q") String city_name, @Query("appid") String apiKey, @Query("units") String units);
}