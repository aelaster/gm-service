package com.gm.server.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SysInfo {
    @SerializedName("country")
    @Expose
    private String country;

    public String getCountry() {
        return country;
    }
}