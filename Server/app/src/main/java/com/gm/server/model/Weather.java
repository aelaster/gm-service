package com.gm.server.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Weather {

    @SerializedName("cod")
    @Expose
    private String http_code = "";

    @SerializedName("message")
    @Expose
    private String http_message = "";

    @SerializedName("name")
    @Expose
    private String city_name = "";

    @SerializedName("weather")
    @Expose
    private ArrayList<WeatherInfo> weatherInfo;

    @SerializedName("main")
    @Expose
    private TemperatureInfo temperatureInfo;

    @SerializedName("sys")
    @Expose
    private SysInfo sysInfo;

    public String getCityName() {
        return city_name;
    }

    public ArrayList<WeatherInfo> getWeatherInfo() {
        return weatherInfo;
    }

    public TemperatureInfo getTemperatureInfo() {
        return temperatureInfo;
    }

    public SysInfo getSysInfo() {
        return sysInfo;
    }

    public String getHttp_code() {
        return http_code;
    }

    public void setHttp_code(String http_code) {
        this.http_code = http_code;
    }

    public String getHttp_message() {
        return http_message;
    }

    public void setHttp_message(String http_message) {
        this.http_message = http_message;
    }
}

