package com.gm.server.api;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Singleton
@Module
public class WeatherModule {
    @Provides
    public WeatherAPIInteractor providesWeatherAPIInteractor() {
        return new WeatherAPIInteractorImpl();
    }
}