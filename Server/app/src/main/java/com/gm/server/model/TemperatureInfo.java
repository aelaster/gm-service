package com.gm.server.model;

import android.content.Context;

import com.gm.server.R;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TemperatureInfo {
    @SerializedName("temp")
    @Expose
    private double temperature;

    public String getTemprature(Context context) {
        int theTemp = (int) Math.round(temperature);
        return context.getString(R.string.degrees, theTemp);
    }
}