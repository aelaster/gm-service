# GM Service Demo
This demo contains the 2 separate apps, AIDL Client and AIDL Server. 

The Server app uses Retrofit to connect to https://openweathermap.org/current and retrieves the current city, temprature, conditions, and weather icon for a given latitude and longitude or a given city name.

The Client app connects to the Server app, retrieves the current weather, and displays it.

The Server app should be installed before the Client app.